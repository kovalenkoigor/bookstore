<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function all()
    {
        $books = Book::with('author')->paginate(9);
        return response()->json([
            "books" => $books
        ], 200);
    }
    public function getBook($id)
    {
        $book = Book::find($id);
        return response()->json([
            "book" => $book
        ], 200);
    }
    public function getAuthor($id)
    {
        $book = Book::with('author')->where('author_id', $id)->paginate(15);
        return response()->json([
            'books' => $book
        ], 200);
    }
    public function getBooksAZ()
    {
        $books = Book::with('author')->orderBy('title', 'asc')->paginate(15);
        return response()->json([
            "books" => $books
        ], 200);
    }
    public function getBooksZA()
    {
        $books = Book::with('author')->orderBy('title', 'desc')->paginate(15);
        return response()->json([
            "books" => $books
        ], 200);
    }
    public function getAuthorsAZ()
    {
        $books = Book::with('author')
            ->select('books.*')
            ->join('authors', 'authors.id', '=', 'books.author_id')
            ->orderBy('authors.name')->paginate(15);

        return response()->json([
            "books" => $books
        ], 200);
    }

    public function getAuthorsZA()
    {
        $books = Book::with('author')
            ->select('books.*')
            ->join('authors', 'authors.id', '=', 'books.author_id')
            ->orderBy('authors.name', 'desc')->paginate(15);
        return response()->json([
            "books" => $books
        ], 200);
    }

}
