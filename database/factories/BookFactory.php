<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'author_id' => $faker->numberBetween($min = 1, $max = 50),
        'description' => $faker->paragraph(rand(2, 10), true),
        'year' => $faker->numberBetween($min = 1995, $max = 2018),
    ];
});
