<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Author::class, 50)->create()->each(function ($a) {
        for ($i=0; $i < rand(1,5); $i++) {
          $a->books()->save(factory(App\Book::class)->make());
        }
      });
    }
}
