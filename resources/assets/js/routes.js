// Books imports
import  BooksMain from './components/books/Main'
import  BooksList from './components/books/List'
import  BooksA from './components/books/BooksA'
import  BooksZ from './components/books/BooksZ'
import  AuthorsA from './components/books/AuthorsA'
import  AuthorsZ from './components/books/AuthorsZ'
import  Book from './components/books/BookView'
import  Author from './components/books/AuthorView'

export const routes = [
  {
    path: '/',
    component: BooksMain,
    meta: {
      requiresAuth: false
    },
    children: [
      {
        path: '/',
        component: BooksList
      },
      {
        path: '/books-a-z',
        component: BooksA
      },
      {
        path: '/books-z-a',
        component: BooksZ
      },
      {
        path: '/authors-a-z',
        component: AuthorsA
      },
      {
        path: '/authors-z-a',
        component: AuthorsZ
      },
      {
        path: '/book/:id',
        component: Book
      },
      {
        path: '/author/:id',
        component: Author
      },
    ]
  }
];