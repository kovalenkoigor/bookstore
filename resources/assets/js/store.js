import {getLocalUser} from "./helpers/auth";

const user = getLocalUser();

export default {
  state: {
    currentUser: user,
    isLoggedIn: !!user,
    loading: false,
    auth_error: null,
    books: [],
    welcomeMessage: 'Welcome'
  },
  getters: {
    isLoading(state) {
      return state.loading;
    },
    isLoggedIn(state) {
      return state.isLoggedIn;
    },
    currentUser(state) {
      return state.currentUser;
    },
    authError(state) {
      return state.authError;
    },
    books(state) {
      return state.books;
    },
    welcome (state) {
      return state.welcomeMessage;
    }
  },
  mutations: {
    login(state) {
      state.login = true;
      state.auth_error = null;
    },
    loginSuccess(state, payload) {
      state.auth_error = null;
      state.isLoggedIn = true;
      state.loading = false;
      state.currentUser = Object.assign({},payload.user, {token: payload.access_token});

      localStorage.setItem("user", JSON.stringify(state.currentUser));
    },
    loginFailed(state, payload) {
      state.loading = false;
      state.auth_error = payload.error;
    },
    logout(state) {
      localStorage.removeItem("user");
      state.isLoggedIn = false;
      state.currentUser = null;
    },
    updateBooks(state, payload) {
      state.books = payload;
    }
  },
  actions: {
    login(context) {
      context.commit("login");
    },
    getBooks(context) {
      axios.get('/api/books', {
        headers: {
          "Authorization": `Bearer ${context.state.currentUser.token}`
        }
      })
          .then((response) => {
            context.commit('updateBooks', response.data.books)
          })
    },
    getBooksAZ(context) {
      axios.get('/api/books/books-a-z', {
        headers: {
          "Authorization": `Bearer ${context.state.currentUser.token}`
        }
      })
          .then((response) => {
            context.commit('updateBooks', response.data.books)
          })
    },
    getBooksZA(context) {
      axios.get('/api/books/books-z-a', {
        headers: {
          "Authorization": `Bearer ${context.state.currentUser.token}`
        }
      })
          .then((response) => {
            context.commit('updateBooks', response.data.books)
          })
    },
    getAuthorsAZ(context) {
      axios.get('/api/books/authors-a-z', {
        headers: {
          "Authorization": `Bearer ${context.state.currentUser.token}`
        }
      })
          .then((response) => {
            context.commit('updateBooks', response.data.books)
          })
    },
    getAuthorsZA(context) {
      axios.get('/api/books/authors-z-a', {
        headers: {
          "Authorization": `Bearer ${context.state.currentUser.token}`
        }
      })
          .then((response) => {
            context.commit('updateBooks', response.data.books)
          })
    }
  }

}