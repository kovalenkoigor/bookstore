<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

// Route::group(['prefix' => 'auth'], function ($router) {

//     Route::post('login', 'AuthController@login');
//     Route::post('logout', 'AuthController@logout');
//     Route::post('refresh', 'AuthController@refresh');
//     Route::post('me', 'AuthController@me');

// });


Route::get('books', 'BooksController@all');
Route::get('books/book/author/{id}', 'BooksController@getAuthor');

Route::get('books/book/{id}', 'BooksController@getBook');
Route::get('books/books-a-z', 'BooksController@getBooksAZ');
Route::get('books/books-z-a', 'BooksController@getBooksZA');
Route::get('books/authors-a-z', 'BooksController@getAuthorsAZ');
Route::get('books/authors-z-a', 'BooksController@getAuthorsZA');

